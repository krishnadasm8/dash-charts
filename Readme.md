# What is this ? 

Showing a Dashboard Chart with values

# Installation

`npm i dash-charts --save`

Then...

```
import dashboardChart from 'dash-charts';


dashboardChart({
  items: {
    "categories": [{
        "id": 1,
        "name": "Visit",
        "details": "This is the new description",
        "sub":[{"id":"1","name":"Personalized Video"},{"id":"2","name":"Text"},{"id":"3","name":"Phone"},]
      },
      {
        "id": 2,
        "name": "be back",
        "sub":[{"id":"1","name":"Be back Sub"},{"id":"2","name":"Text"}],
        "details": "Visits are any visit during the Time Filter. You can only be a visit or a be back. They have to be unique so we don’t count 1 customer twice here"
      },
      {
        "id": 3,
        "name": "Proposal",
        "sub":[{"id":"1","name":"Proposal sub"},{"id":"2","name":"Text"},{"id":"3","name":"Phone"}],
        "details": "New details for proposal"
      },
      {
        "id": 4,
        "name": "Work",
        "sub":[{"id":"1","name":"Work sub"}],
        "details": "Details for the work"
      }
    ],
    "users": [{
        "id":1,
        "data":[{
          "categoryId":3,
          "subCategoryId":1,
          "percentage":25
        },{
          "categoryId":3,
          "subCategoryId":3,
          "percentage":80
        },{
          "categoryId":1,
          "subCategoryId":2,
          "percentage":100
        },{
          "categoryId":1,
          "subCategoryId":3,
          "percentage":15
        },{
          "categoryId":2,
          "subCategoryId":2,
          "percentage":30
        }],
        "name": "Mia"
      },
      {
        "id":2,
        "data":[{
          "categoryId":1,
          "subCategoryId":1,
          "percentage":20
        },{
          "categoryId":1,
          "subCategoryId":3,
          "percentage":35
        }],
        "name": "James"
      },
      {
        "id":3,
        "data":[{
          "categoryId":2,
          "subCategoryId":1,
          "percentage":55
        },{
          "categoryId":3,
          "subCategoryId":3,
          "percentage":55
        },{
          "categoryId":3,
          "subCategoryId":1,
          "percentage":100
        }],
        "name": "Team"
      }
    ]
  },
  target: 'target'
})

<div id="target">
</div>

```

## Options

Dash-charts has 2 params:

* *items* - _object_
* *target* - _target-div_
