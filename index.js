import './css/bootstrap.min.css';
import './css/style.css';
import './css/font.css';

const usersList = (users) => {
  let main = ` <div class="bottom-btns">`
  users.users.forEach((element, index) => {
    const op = userItem(element, users, index);
    main = main.concat(op);
  });
  main = main.concat("</div>");
  return main
}

const userItem = (item, users, index) => {
  return `<div id="users${index+1}" style="cursor:pointer" class="orange-list">
<span   class="dark-orange-btn" style="background:${item.color}; border:1px solid #5b5959"></span>
<p>${item.name}</p></div>`
}

const detailsButtons = (details, index) => {
  let main = `<div class="detail-btns d-flex" id=show-details${index}>`
  details.forEach(element => {
    element.data && element.data.forEach(val => {
      if (val.categoryId === index) {
        const op = detailsItem(val, element, index);
        main = main.concat(op);
      }
    })
  });
  main = main.concat("</div>");
  return main
}


const detailsItem = (item, element) => {
  return `<span class="light-orange-btn color${element.id}" 
style="position:absolute; 
left:${item.percentage}%;
background:${element.color}; 
border:1px solid #5b5959"></span>`
}

const expandedDetailsItem = (item, element, top) => {
  console.log('item', item)
  let newItem = item
  const dots = []
  dots.push(newItem)
  return `<span class="light-orange-btn color${element.id}" 
style="position:absolute;
top:${top*32}%;
left:${item.percentage}%;
background:${element.color}; 
border:1px solid #5b5959"></span>`
}

const rowsList = (list, index) => {
  let main = `<div style="min-height:89px" class="detail-view 
${list.categories.length !== index ? 'down-arrow' : ''} be-backs-details">`
  list.categories.forEach(element => {
    if (element.id == index) {
      const op = detailedRows(element, list.users, index);
      main = main.concat(op);
    }
  });
  main = main.concat("</div>");
  return main
}

const detailedRows = (item, data, index) => {
  // return `${detailsButtons(data,index)}`
  return `
<div class="detail-content">
</div>
${detailsButtons(data,index)}
`
}

const showNames = (item, index, items) => {
  if (item.sub) {
    document.getElementById(`show${index}`).onclick = function changeContent() {
      let allNodes = document.querySelectorAll('.detail-view')[index + 1]
      if (items.categories.length > index + 1) {
        if(item.sub.length === 1){
          allNodes.style = "margin-top:-44px"
        }else if(item.sub.length === 2){
          allNodes.style = "margin-top:9px"
        }else{
          for(let i=1;i<item.sub.length;i++){
            allNodes.style = `margin-top:${i*40}px`
          }
        }
      }
      items.users.map((v,key) => {
          let elements = document.querySelectorAll(`.color${(v.id)}`);
          for (var i = 0; i < elements.length; i++) {
            elements[i].style.opacity = '1'
          }
          document.getElementById(`users${(v.id)}`).style.opacity = '1'
      })
      document.getElementById(`show${index}`).innerHTML = expandedNames(item, index);
      let el = document.getElementById(`show-details${index+1}`);
      let newEl = document.createElement('div');
      newEl.className = "detail-btns d-flex"
      newEl.id = `hide-details${index+1}`
      newEl.style = "height:150px"
      newEl.innerHTML = items.users.map((val,key) => {
        return val.data.map((v,i) => {
          if(v.categoryId==index+1){
            return expandedDetailsItem(v, val, v.subCategoryId)
          }
        }).join('');
      }).join('');
      el.parentNode.replaceChild(newEl, el);
      hideNames(item, index, items)
    }
  }
}

const hideNames = (item, index, items) => {
  document.getElementById(`show${index}`).onclick = function changeContent() {
    document.getElementById(`show${index}`).innerHTML = `<button type="btn" class="btn green ${index === 0 ? 'first-arrow-none':''}">${item.name}</button>`
    items.users.map((v,key) => {
          let elements = document.querySelectorAll(`.color${(v.id)}`);
          for (var i = 0; i < elements.length; i++) {
            elements[i].style.opacity = '1'
          }
          document.getElementById(`users${(v.id)}`).style.opacity = '1'
      })
    let allNodes = document.querySelectorAll('.detail-view')[index + 1]
    if (items.categories.length > index + 1) {
      allNodes.style = "margin-top:0px"
      allNodes.style = "min-height:89px"
    }
    let el = document.getElementById(`hide-details${index+1}`);
    let newEl = document.createElement('div');
    newEl.className = "detail-btns d-flex"
    newEl.id = `show-details${index+1}`
    newEl.innerHTML = items.users.map(element => {
      return element.data && element.data.map(val => {
        if (val.categoryId == index + 1) {
          return detailsItem(val, element, index + 1);
        }
      }).join('');
    }).join('');
    el.parentNode.replaceChild(newEl, el);
    showNames(item, index, items)
  }
}

const expandedNames = (item, index) => {
  let main = `<div class="visit position-relative mb-6" >
  <button type="btn" id="hide${index}" class="btn green ${index === 0 ? 'first-arrow-none':''}">${item.name}</button>`
  item.sub && item.sub.forEach((element, index) => {
    const op = `<div class="label" style="top:${(index+1)*70}px; right: 0;">${element.name}</div>`
    main = main.concat(op);
  });
  main = main.concat("</div>");
  return main
}

const namesList = (list) => {
  let main = `<div class="bd-slide">`
  list.forEach((element, index) => {
    const op = namesRows(element, index);
    main = main.concat(op);
  });
  main = main.concat("</div>");
  return main
}

const namesRows = (item, index) => {
  console.log('item.details', item.details)
  return ` <div id="show${index}" class="visit mb-6" style="--test:'${item.details}'">
<button type="btn" class="btn green ${index === 0 ? 'first-arrow-none':''}">${item.name}</button>
</div>`
}



const dashboardChart = ({
  target,
  items
} = {}) => {

  const getRandomColor = () => {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
  items.users.forEach(element => {
    element.color = getRandomColor()
  });

  const html = `
        <div class="mainDiv">
            <div class="wrapper">
                <div class="container-fluid container-1500 mb-3">
                    <div class="dashboard-inner">
                    ${namesList(items.categories)}
                        <div class="bd-content">
                            <div class="graphHeader">
                            <div class="ratio-rate ratio-1 d-flex justify-content-between">
                            <div class=" ratio-0"><span>0%</span></div>
                            <div class=" ratio-1-SUB"><span>10%</span></div>
                            </div>
                            <div class="ratio-rate ratio-2 d-flex justify-content-end"><span>20%</span></div>
                            <div class="ratio-rate ratio-3 d-flex justify-content-end "><span>30%</span></div>
                            <div class="ratio-rate ratio-4 d-flex justify-content-end"><span>40%</span></div>
                            <div class="ratio-rate ratio-5 d-flex justify-content-end"><span>50%</span></div>
                            <div class="ratio-rate ratio-6 d-flex justify-content-end"><span>60%</span></div>
                            <div class="ratio-rate ratio-7 d-flex justify-content-end"><span>70%</span></div>
                            <div class="ratio-rate ratio-8 d-flex justify-content-end"><span>80%</span></div>
                            <div class="ratio-rate ratio-9 d-flex justify-content-end"><span>90%</span></div>
                            <div class="ratio-rate ratio-10 d-flex justify-content-end"><span>100%</span></div>
                            </div>
                            <div class="mt-3 height-01">
                            ${items.categories.length && items.categories.map((val,index) =>{
                            return rowsList(items,index+1)
                            }).join('')}
                            </div>
                            ${usersList(items)}
                            </div>
                    </div>
                </div>`
  document.getElementById(target).innerHTML = html;
  items.categories.forEach((element, index) => {
    showNames(element, index, items)
  });

  items.users.map((val, key) => {
    document.getElementById(`users${key+1}`).addEventListener("click", function () {
      items.users.filter(item => item.id != key + 1).map((v, i) => {
        let elements = document.querySelectorAll(`.color${(v.id)}`);
        for (var i = 0; i < elements.length; i++) {
          elements[i].style.opacity = '0.3'
        }
        document.getElementById(`users${(v.id)}`).style.opacity = '0.3'
      })
      items.users.filter(item => item.id == key + 1).map((v, i) => {
        let elements = document.querySelectorAll(`.color${(v.id)}`);
        for (var i = 0; i < elements.length; i++) {
          elements[i].style.opacity = '1'
        }
        document.getElementById(`users${(v.id)}`).style.opacity = '1'
      })
    });
  })
  return html;
};

export default dashboardChart;